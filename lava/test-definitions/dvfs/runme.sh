#!/bin/sh

set +x

frequencies="$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies)"
echo "<LAVA_SIGNAL_STARTTC dvfs-frequencies>"
if [ "$frequencies" = "2000000 2100000 2200000 2300000 2400000 " ]; then
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=dvfs-frequencies RESULT=pass>"
else
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=dvfs-frequencies RESULT=fail>"
fi
echo "<LAVA_SIGNAL_ENDTC dvfs-frequencies>"

governors="$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors)"
echo "<LAVA_SIGNAL_STARTTC dvfs-governors>"
if [ "$governors" = "conservative powersave performance schedutil " ]; then
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=dvfs-governors RESULT=pass>"
else
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=dvfs-governors RESULT=fail>"
fi
echo "<LAVA_SIGNAL_ENDTC dvfs-governors>"

echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
sleep 1
cur_freq=$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq)
echo "<LAVA_SIGNAL_STARTTC dvfs-performance>"
if [ "$cur_freq" -eq 2400000 ]; then
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=dvfs-performance RESULT=pass>"
else
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=dvfs-performance RESULT=fail>"
fi
echo "<LAVA_SIGNAL_ENDTC dvfs-performance>"

echo powersave > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
sleep 1
cur_freq=$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq)
echo "<LAVA_SIGNAL_STARTTC dvfs-powersave>"
if [ "$cur_freq" -eq 2000000 ]; then
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=dvfs-powersave RESULT=pass>"
else
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=dvfs-powersave RESULT=fail>"
fi
echo "<LAVA_SIGNAL_ENDTC dvfs-powersave>"

echo schedutil > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
sleep 1
cur_freq=$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq)
echo "<LAVA_SIGNAL_STARTTC dvfs-schedutil>"
if [ "$cur_freq" -eq 2000000 ]; then
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=dvfs-schedutil RESULT=pass>"
else
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=dvfs-schedutil RESULT=fail>"
fi
echo "<LAVA_SIGNAL_ENDTC dvfs-schedutil>"
