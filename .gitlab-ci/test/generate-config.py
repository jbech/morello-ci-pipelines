import fnmatch
import os

tests = []
gtest_filters = [
    "-string.*:string_nofortify.*",
    "string_nofortify.*-string_nofortify.strlcat_overread:string_nofortify.bcopy:string_nofortify.memmove",
    "string_nofortify.strlcat_overread:string_nofortify.bcopy:string_nofortify.memmove",
    "string.*-string.strlcat_overread:string.bcopy:string.memmove",
    "string.strlcat_overread:string.bcopy:string.memmove",
]

templates = os.listdir('lava/templates')
pattern = "fvp-android-*.yaml"
for fn in templates:
    if fnmatch.fnmatch(fn, pattern):
        test = fn.removeprefix("fvp-").removesuffix(".yaml")
        tests.append(test)

user_defined_tests = os.environ.get("USER_DEFINED_TESTS")
if user_defined_tests is None:
    print("INFO: Use default tests", *tests, sep="\n* ")
else:
    tests.clear()
    user_defined_tests = user_defined_tests.strip("'")
    tests = user_defined_tests.split()
    print("INFO: Use specified tests", *tests, sep="\n* ")
print("")

with open("config-values.yml", "w") as fp:
    fp.write("#@data/values\n")
    fp.write("---\n")
    fp.write("build_job_id: {}\n".format(os.environ.get("BUILD_JOB_ID")))
    fp.write("services: [")
    for test in tests:
        if "android-bionic" not in test:
            fp.write("'{}', ".format(test))
    fp.write("]\n")
    fp.write("bionic: [")
    if "android-bionic" in tests:
        for gtest_filter in gtest_filters:
            fp.write("'{}', ".format(gtest_filter))
    fp.write("]\n")

with open("config-values.yml") as fp:
    print("INFO: config-values.yaml generated")
    for line in fp:
        print(line, end='')
