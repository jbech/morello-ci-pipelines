#!/bin/sh

#git submodule sync --recursive
#git submodule update --init --recursive

TF_A_URL=${TF_A_URL:-}

install_custom_tf_a()
{
  test -z "${TF_A_URL}" && \
    curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo bl31.bin \
    https://git.morello-project.org/morello/trusted-firmware-a/-/jobs/artifacts/morello/master/raw/bl31.bin?job=build-tf-a
  test -f bl31.bin || \
    curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo bl31.bin ${TF_A_URL}
}

install_custom_tf_a

which arm-none-eabi-gcc
arm-none-eabi-gcc --version
which fiptool
fiptool version

set -ex

make -j $(nproc) \
  V=1 \
  PRODUCT=morello \
  MODE=release \
  LOG_LEVEL=INFO \
  CC=arm-none-eabi-gcc \
  AR=arm-none-eabi-ar \
  SIZE=arm-none-eabi-size \
  OBJCOPY=arm-none-eabi-objcopy

cp -a build/product/morello/*/release/bin/*cp_*.bin ${CI_PROJECT_DIR}
ln -sf scp_romfw.bin scp_rom.bin
ln -sf mcp_romfw.bin mcp_rom.bin
fiptool create --scp-fw scp_ramfw_fvp.bin --soc-fw bl31.bin scp_fw.bin
fiptool create --blob uuid=54464222-a4cf-4bf8-b1b6-cee7dade539e,file=mcp_ramfw_fvp.bin mcp_fw.bin

# Create SHA256SUMS.txt file
sha256sum *.bin > SHA256SUMS.txt
